package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Navigate to ConcertsActivity screen (list of concerts) when clicked radConcerts button
        RadioButton radBtnConcerts = (RadioButton) findViewById(R.id.radConcerts);
        View.OnClickListener btnOneListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentOne = new Intent(ListActivity.this,ConcertsActivity.class);
                startActivity(intentOne);

            }
        };
        radBtnConcerts.setOnClickListener(btnOneListener);

        // Navigate to SportsActivity screen (list of sports) when clicked radSports button
        RadioButton radBtnSports = (RadioButton)findViewById(R.id.radSports);
        View.OnClickListener btnTwoListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentTwo = new Intent(ListActivity.this, SportsActivity.class);
                startActivity(intentTwo);
            }
        };
        radBtnSports.setOnClickListener(btnTwoListener);

        // Navigate to MoviesActivity screen (list of movies) when clicked radMovies button
        RadioButton radBtnMovies = (RadioButton)findViewById(R.id.radMovies);
        View.OnClickListener btnThreeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentThree = new Intent(ListActivity.this, MoviesActivity.class);
                startActivity(intentThree);
            }
        };
        radBtnMovies.setOnClickListener(btnThreeListener);

    }
}