package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Navigate to the List Activity screen when clicked on the Main Button
        Button btnTitle = (Button)findViewById(R.id.btnMain);
        View.OnClickListener btnTitleListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent titleIntent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(titleIntent);
            }
        };
        btnTitle.setOnClickListener(btnTitleListener);
    }
}