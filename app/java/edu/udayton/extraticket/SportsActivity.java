package edu.udayton.extraticket;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.List;

public class SportsActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> Sports = Arrays.asList(getResources().getStringArray(R.array.sport));
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_sports, R.id.sportsList,Sports));
    }

    // Handling list of sports with switch
    protected void onListItemClick(ListView i, View v,int position, long id)
    {
        Intent itemIntent;
        switch (position) {
            case 0:
                itemIntent = new Intent(SportsActivity.this, BookGameActivity.class);
                itemIntent.putExtra(BookGameActivity.LBL_KEY, getResources().getString(R.string.sportOne));
                itemIntent.putExtra(BookGameActivity.ID_KEY, Integer.toString(R.drawable.png_wimbledon));
                break;
            case 1:
                itemIntent = new Intent(SportsActivity.this, BookGameActivity.class);
                itemIntent.putExtra(BookGameActivity.LBL_KEY, getResources().getString(R.string.sportTwo));
                itemIntent.putExtra(BookGameActivity.ID_KEY, Integer.toString(R.drawable.png_ud_vs_vcu));
                break;
            case 2:
                itemIntent = new Intent(SportsActivity.this, BookGameActivity.class);
                itemIntent.putExtra(BookGameActivity.LBL_KEY, getResources().getString(R.string.sportThree));
                itemIntent.putExtra(BookGameActivity.ID_KEY, Integer.toString(R.drawable.png_ind_vs_aus));
                break;
            default:
                Toast myToast = Toast.makeText(SportsActivity.this,
                        "Invalid Choice Made", Toast.LENGTH_LONG);
                myToast.show();

                itemIntent = null;
                break;
        } // end switch

        // start the activity using the itemIntent only if there is one
        if (itemIntent != null)
        {
            startActivity(itemIntent);
        }
    } // end onListItemClick
} // end SportsActivity class

