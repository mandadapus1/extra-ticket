package edu.udayton.extraticket;

import io.realm.Realm;
import io.realm.mongodb.User;
import io.realm.mongodb.sync.SyncConfiguration;

public class BackgroundQuickStart implements Runnable {
    User user;
    UserDetails loginDetails;
    RealmCallback<UserDetails> callback;
    public BackgroundQuickStart(User user, UserDetails loginDetails, RealmCallback<UserDetails> callback) {
        this.user = user;
        this.loginDetails = loginDetails;
        this.callback = callback;
    }

    @Override
    public void run() {
        String partitionValue = "ExtraTicket";
        SyncConfiguration config = new SyncConfiguration.Builder(user, partitionValue)
                .allowQueriesOnUiThread(true)
                .allowWritesOnUiThread(true)
                .build();
        Realm backgroundThreadRealm = Realm.getInstance(config);

        backgroundThreadRealm.executeTransaction(r -> {
            r.insert(loginDetails);
        });

        UserDetails userDetails = backgroundThreadRealm.where(UserDetails.class)
                .equalTo("userName", loginDetails.getUserName())
                .findFirst();

        callback.onComplete(userDetails);

        backgroundThreadRealm.close();
    }
}
