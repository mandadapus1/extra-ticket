package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Button btnLogin = (Button) findViewById(R.id.btnRegister);

        EditText txtName   = (EditText)findViewById(R.id.txtName);
        EditText txtEmail   = (EditText)findViewById(R.id.txtEmail);
        EditText txtUserName   = (EditText)findViewById(R.id.txtUserName);
        EditText txtPwd   = (EditText)findViewById(R.id.txtPwd);

        View.OnClickListener btnRegisterListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserDetails registerDetails = new UserDetails(
                        txtName.getText().toString(),
                        txtEmail.getText().toString(),
                        txtUserName.getText().toString(),
                        txtPwd.getText().toString());

                ExecutorService executorService = Executors.newFixedThreadPool(2);
                FutureTask<String> task = new FutureTask(new RegisterBackGroundRunnable(
                        registerDetails, (RealmCallback<UserDetails>) userDetails -> {
                    if (userDetails == null) {
                        // Happy path
                        Log.v("Success", "Registered..!");
                    } else {
                        // Show error in UI
                        Log.v("Found", "User name already taken..!");
                    }
                }), null);

                executorService.submit(task);

                while (true) {
                    if (task.isDone()) {
                        //shut down executor service
                        executorService.shutdown();
                        return;
                    }
                }
            }
        };
        btnLogin.setOnClickListener(btnRegisterListener);
    }
}