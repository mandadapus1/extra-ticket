package edu.udayton.extraticket;

import android.app.Application;
import android.util.Log;

import io.realm.Realm;
import io.realm.mongodb.App;
import io.realm.mongodb.AppConfiguration;
import io.realm.mongodb.Credentials;
import io.realm.mongodb.User;
import io.realm.mongodb.sync.SyncConfiguration;

public class RealmApplication extends Application {
    App app;

    @Override
    public void onCreate() {
        super.onCreate();

        // The Realm file will be located in package's "files" directory.
        // RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
        Realm.init(this); // context, usually an Activity or Application
        Log.v("QUICKSTART", "Successfully authenticated anonymously.");

        app = new App(new AppConfiguration.Builder("extraticket-pnrzm").build());

        Credentials credentials = Credentials.anonymous();

        app.loginAsync(credentials, result -> {
            if (result.isSuccess()) {
                Log.v("QUICKSTART", "Login done");
                User user = app.currentUser();
                String partitionValue = "ExtraTicket";
                SyncConfiguration config = new SyncConfiguration.Builder(user, partitionValue)
                        .allowQueriesOnUiThread(true)
                        .allowWritesOnUiThread(true)
                        .build();
                Realm.setDefaultConfiguration(config);
            }
        });
    }
}

