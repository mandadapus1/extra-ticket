package edu.udayton.extraticket;

import io.realm.Realm;
import io.realm.mongodb.User;
import io.realm.mongodb.sync.SyncConfiguration;

public class LoginBackGroundRunnable implements Runnable {
    User user;
    UserDetails loginDetails;
    RealmCallback<UserDetails> callback;

    public LoginBackGroundRunnable(UserDetails loginDetails, RealmCallback<UserDetails> callback) {
        this.loginDetails = loginDetails;
        this.callback = callback;
    }

    @Override
    public void run() {
        Realm backgroundThreadRealm = Realm.getDefaultInstance();

        UserDetails userDetails = backgroundThreadRealm.where(UserDetails.class)
                .equalTo("userName", loginDetails.getUserName())
                .equalTo("password", loginDetails.getPassword())
                .findFirst();

        callback.onComplete(userDetails);

        backgroundThreadRealm.close();
    }
}
