package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;


public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText txtUserName   = (EditText)findViewById(R.id.txtUserName);
        EditText txtPwd   = (EditText)findViewById(R.id.txtPwd);

        Button btnLogin = (Button)findViewById(R.id.btnSignin);
        View.OnClickListener btnLoginListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserDetails loginDetails = new UserDetails(
                        txtUserName.getText().toString(),
                        txtPwd.getText().toString());

                ExecutorService executorService = Executors.newFixedThreadPool(2);

                FutureTask<String> task = new FutureTask(new LoginBackGroundRunnable(
                        loginDetails, (RealmCallback<UserDetails>) userDetails -> {
                    if (userDetails == null) {
                        // Error Message
                        Log.v("EMPTY", "No Details");
                    } else {
                        // Happy path
                        Log.v("Found", userDetails.getUserName());
                        Intent titleIntent = new Intent(Login.this, ListActivity.class);
                        startActivity(titleIntent);
                    }
                }), null);

                executorService.submit(task);

                while (true) {
                    if (task.isDone()) {
                        //shut down executor service
                        executorService.shutdown();
                        return;
                    }
                }
            }
        };
        btnLogin.setOnClickListener(btnLoginListener);
    }
}