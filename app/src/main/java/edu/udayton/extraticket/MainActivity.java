package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Navigate to the List Activity screen when clicked on the Main Button
        Button btnTitle = (Button)findViewById(R.id.btnMain);
        View.OnClickListener btnTitleListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent titleIntent = new Intent(MainActivity.this, ListActivity.class);
                startActivity(titleIntent);
            }
        };
        btnTitle.setOnClickListener(btnTitleListener);

        // Navigate to the Login screen when clicked on the Login Button
        Button btnLogin = (Button)findViewById(R.id.btnLogin);
        View.OnClickListener btnLoginListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent titleIntent = new Intent(MainActivity.this, Login.class);
                startActivity(titleIntent);
            }
        };
        btnLogin.setOnClickListener(btnLoginListener);

        // Navigate to the Register screen when clicked on the Register Button
        Button btnRegisterMain = (Button)findViewById(R.id.btnRegisterMain);
        View.OnClickListener btnRegMainListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent titleIntent = new Intent(MainActivity.this, Register.class);
                startActivity(titleIntent);
            }
        };
        btnRegisterMain.setOnClickListener(btnRegMainListener);
    }
}