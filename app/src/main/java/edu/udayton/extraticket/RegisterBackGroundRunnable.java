package edu.udayton.extraticket;

import io.realm.Realm;
import io.realm.mongodb.User;
import io.realm.mongodb.sync.SyncConfiguration;

public class RegisterBackGroundRunnable implements Runnable {
    User user;
    UserDetails registerDetails;
    RealmCallback<UserDetails> callback;

    public RegisterBackGroundRunnable(UserDetails registerDetails, RealmCallback<UserDetails> callback) {
        this.registerDetails = registerDetails;
        this.callback = callback;
    }

    @Override
    public void run() {
        Realm backgroundThreadRealm = Realm.getDefaultInstance();

        UserDetails userDetails = backgroundThreadRealm.where(UserDetails.class)
                .equalTo("userName", registerDetails.getUserName())
                .findFirst();

        if (userDetails == null) {
            backgroundThreadRealm.executeTransaction(r -> {
                r.insert(registerDetails);
            });
        }

        callback.onComplete(userDetails);

        backgroundThreadRealm.close();
    }
}

