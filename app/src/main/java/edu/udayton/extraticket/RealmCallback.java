package edu.udayton.extraticket;

interface RealmCallback<T> {
    void onComplete(UserDetails result);
}
