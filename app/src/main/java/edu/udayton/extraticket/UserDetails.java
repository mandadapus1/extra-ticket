package edu.udayton.extraticket;

import org.bson.types.ObjectId;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class UserDetails extends RealmObject {
    private String name;
    @PrimaryKey
    private ObjectId _id = new ObjectId();
    @Required
    private String email;
    @Required private String userName;
    @Required private String password;

    public UserDetails(String name, String email, String userName, String password) {
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.name = name;
    }

    public UserDetails(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public UserDetails() {}

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ObjectId get_id() {
        return _id;
    }

    public void set_id(ObjectId _id) {
        this._id = _id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
         return userName;
    }
}
