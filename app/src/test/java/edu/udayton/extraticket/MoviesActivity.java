package edu.udayton.extraticket;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.List;

public class MoviesActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> Movies = Arrays.asList(getResources().getStringArray(R.array.movie));
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_movies, R.id.moviesList,Movies));
    }

    // Handling list of movies with switch
    protected void onListItemClick(ListView i, View v,int position, long id)
    {
        Intent itemIntent;
        switch (position) {
            case 0:
                itemIntent = new Intent(MoviesActivity.this, BookMovieActivity.class);
                itemIntent.putExtra(BookMovieActivity.LBL_KEY, getResources().getString(R.string.movieOne));
                itemIntent.putExtra(BookMovieActivity.ID_KEY, Integer.toString(R.drawable.png_hit));
                break;
            case 1:
                itemIntent = new Intent(MoviesActivity.this, BookMovieActivity.class);
                itemIntent.putExtra(BookMovieActivity.LBL_KEY, getResources().getString(R.string.movieTwo));
                itemIntent.putExtra(BookMovieActivity.ID_KEY, Integer.toString(R.drawable.png_bheeshma));
                break;
            case 2:
                itemIntent = new Intent(MoviesActivity.this, BookMovieActivity.class);
                itemIntent.putExtra(BookMovieActivity.LBL_KEY, getResources().getString(R.string.movieThree));
                itemIntent.putExtra(BookMovieActivity.ID_KEY, Integer.toString(R.drawable.png_jaanu));
                break;
            default:
                Toast myToast = Toast.makeText(MoviesActivity.this,
                        "Invalid Choice Made", Toast.LENGTH_LONG);
                myToast.show();

                itemIntent = null;
                break;
        } // end switch

        // start the activity using the itemIntent only if there is one
        if (itemIntent != null)
        {
            startActivity(itemIntent);
        }
    } // end onListItemClick
} // end MoviesActivity class