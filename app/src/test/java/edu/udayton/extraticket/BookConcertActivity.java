package edu.udayton.extraticket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;
import java.text.DecimalFormat;

public class BookConcertActivity extends AppCompatActivity {
    public static final String ID_KEY = "RES_ID";
    public static final String LBL_KEY = "LABEL";

    private final double eachTicketCost = 149.99;
    private int numberOfTickets;
    private double totalCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_concert);

        Intent itemIntent = getIntent();
        Bundle myExtras = itemIntent.getExtras();

        // Handling Concert Title, Image and External link Button in the BookConcertActivity screen
        if(myExtras != null) {
            final String res_label = myExtras.getString(LBL_KEY);

            final TextView textView2 = (TextView) findViewById(R.id.concertTitleText);
            textView2.setText(res_label);

            String image_id = myExtras.getString(ID_KEY);

            int imageId = Integer.parseInt(image_id);

            final ImageView picture = (ImageView) findViewById(R.id.imgConcert);
            picture.setImageResource(imageId);
            picture.setContentDescription(res_label);

            Button btnClick = (Button) findViewById(R.id.btnConcertExternalLink);
            View.OnClickListener btnClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = null;

                    if (res_label.equals("Backstreet Boys")) {
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.livenation.com/artist/K8vZ91719tf/backstreet-boys-events"));
                    } else if (res_label.equals("Jennifer Lopez")) {
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.livenation.com/artist/K8vZ917GAT7/jennifer-lopez-events"));
                    }else if (res_label.equals("Shawn Mendes")) {
                        i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.livenation.com/artist/K8vZ91739Nf/shawn-mendes-events"));
                    }
                    startActivity(i);
                }
            };

            btnClick.setOnClickListener(btnClickListener);
        }

        DecimalFormat currencyFormat = new DecimalFormat("$###,###.##");

        // To display each ticket cost
        final TextView ticketCost = (TextView)findViewById(R.id.concertTicketCost);
        String TicketCost = "Each Ticket Cost: " + currencyFormat.format(eachTicketCost);
        ticketCost.setText(TicketCost);

        // set the Confirm button reference
        final Button btnConfirm = (Button)findViewById(R.id.btnConfirm);
        assert btnConfirm != null;

        //set the event listener/handler for btnConfirm button
        View.OnClickListener btnCostListener = new View.OnClickListener() {

            final EditText numberEditText = (EditText)findViewById(R.id.numberEditText);
            final TextView resultTextView = (TextView)findViewById(R.id.resultTextView);

            @Override
            public void onClick(View v) {
                // get number of tickets (user input)
                Editable Input = numberEditText.getText();
                String InputAsString = Input.toString();

                // set up the currency formatter for the total cost output
                DecimalFormat currencyFormat = new DecimalFormat("$###,###.##");

                // use exception handling in case of no input
                try
                {
                    // convert InputAsString to an integer
                    numberOfTickets = Integer.parseInt(InputAsString);

                    // calculate the total cost of the tickets
                    totalCost = eachTicketCost * numberOfTickets;

                    // output the total ticket cost
                    String OutputString = "Total Tickets Cost: " +
                            currencyFormat.format(totalCost);

                    resultTextView.setText(OutputString);
                }
                catch (Exception ex)
                {
                    Log.e(ex.getMessage(), ex.toString());

                } // end try...catch
            } // end onClick handler
        };

        // set btnConfirmListener as btnConfirm's event listener/handler
        btnConfirm.setOnClickListener(btnCostListener);

    } // end onCreate method
} // end BookConcertActivity class