package edu.udayton.extraticket;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.Arrays;
import java.util.List;

public class ConcertsActivity extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> Concerts = Arrays.asList(getResources().getStringArray(R.array.concert));
        setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_concerts, R.id.concertsList,Concerts));
    }

    // Handling list of concerts with switch
    protected void onListItemClick(ListView i, View v,int position, long id)
    {
        Intent itemIntent;
        switch (position) {
            case 0:
                itemIntent = new Intent(ConcertsActivity.this, BookConcertActivity.class);
                itemIntent.putExtra(BookConcertActivity.LBL_KEY, getResources().getString(R.string.concertOne));
                itemIntent.putExtra(BookConcertActivity.ID_KEY, Integer.toString(R.drawable.png_backstreet_boys));
                break;
            case 1:
                itemIntent = new Intent(ConcertsActivity.this, BookConcertActivity.class);
                itemIntent.putExtra(BookConcertActivity.LBL_KEY, getResources().getString(R.string.concertTwo));
                itemIntent.putExtra(BookConcertActivity.ID_KEY, Integer.toString(R.drawable.png_jennifer));
                break;
            case 2:
                itemIntent = new Intent(ConcertsActivity.this, BookConcertActivity.class);
                itemIntent.putExtra(BookConcertActivity.LBL_KEY, getResources().getString(R.string.concertThree));
                itemIntent.putExtra(BookConcertActivity.ID_KEY, Integer.toString(R.drawable.png_shawn_mendes));
                break;
            default:
                Toast myToast = Toast.makeText(ConcertsActivity.this,
                        "Invalid Choice Made", Toast.LENGTH_LONG);
                myToast.show();

                itemIntent = null;
                break;
        } // end switch

        // start the activity using the itemIntent only if there is one
        if (itemIntent != null)
        {
            startActivity(itemIntent);
        }
    } // end onListItemClick
} // end ConcertsActivity class
